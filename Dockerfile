FROM openjdk:8-jre-alpine

RUN wget http://mirrors.fibergrid.in/apache/tomcat/tomcat-8/v8.5.34/bin/apache-tomcat-8.5.34.tar.gz
RUN tar -xvzf apache-tomcat-8.5.34.tar.gz

ENV CATALINA_HOME=/apache-tomcat-8.5.34
ENV PATH=$CATALINA_HOME/bin:$PATH

ADD java-tomcat-maven-example.war /apache-tomcat-8.5.34/webapps

EXPOSE 8080

CMD ["catalina.sh", "run"]